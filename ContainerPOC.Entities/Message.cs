﻿#region Using directives

using System;

#endregion

namespace ContainerPOC.Entities
{	
	///<summary>
	/// An object representation of the 'Message' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Message : MessageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Message"/> instance.
		///</summary>
		public Message():base(){}	
		
		#endregion
	}
}

﻿using System;
using System.ComponentModel;

namespace ContainerPOC.Entities
{
	/// <summary>
	///		The data structure representation of the 'Guests' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IGuests 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "Guests"</remarks>
		System.Int32 Id { get; set; }
				
		
		
		/// <summary>
		/// GuestName : 
		/// </summary>
		System.String  GuestName  { get; set; }
		
		/// <summary>
		/// CreatedBy : 
		/// </summary>
		System.String  CreatedBy  { get; set; }
		
		/// <summary>
		/// CreatedDate : 
		/// </summary>
		System.DateTime?  CreatedDate  { get; set; }
		
		/// <summary>
		/// LastUpdateBy : 
		/// </summary>
		System.String  LastUpdateBy  { get; set; }
		
		/// <summary>
		/// LastUpdateDate : 
		/// </summary>
		System.DateTime?  LastUpdateDate  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}



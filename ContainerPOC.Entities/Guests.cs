﻿#region Using directives

using System;

#endregion

namespace ContainerPOC.Entities
{	
	///<summary>
	/// An object representation of the 'Guests' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Guests : GuestsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Guests"/> instance.
		///</summary>
		public Guests():base(){}	
		
		#endregion
	}
}

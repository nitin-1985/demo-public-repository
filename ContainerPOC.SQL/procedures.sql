
USE [ContainerPOCdb]
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Message_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Message_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Message_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Gets all records from the Message table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Message_Get_List

AS


				
				SELECT
					[Id],
					[Message],
					[CreatedBy],
					[CreatedDate],
					[LastUpdateBy],
					[LastUpdateDate]
				FROM
					[dbo].[Message]
					
				SELECT @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Message_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Message_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Message_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Gets records from the Message table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Message_GetPaged
(

	@WhereClause varchar (8000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[Id]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [Id]'
				SET @SQL = @SQL + ', [Message]'
				SET @SQL = @SQL + ', [CreatedBy]'
				SET @SQL = @SQL + ', [CreatedDate]'
				SET @SQL = @SQL + ', [LastUpdateBy]'
				SET @SQL = @SQL + ', [LastUpdateDate]'
				SET @SQL = @SQL + ' FROM [dbo].[Message]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [Id],'
				SET @SQL = @SQL + ' [Message],'
				SET @SQL = @SQL + ' [CreatedBy],'
				SET @SQL = @SQL + ' [CreatedDate],'
				SET @SQL = @SQL + ' [LastUpdateBy],'
				SET @SQL = @SQL + ' [LastUpdateDate]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(1) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[Message]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Message_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Message_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Message_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Inserts a record into the Message table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Message_Insert
(

	@Id int    OUTPUT,

	@Message varchar (500)  ,

	@CreatedBy varchar (250)  ,

	@CreatedDate datetime   ,

	@LastUpdateBy varchar (250)  ,

	@LastUpdateDate datetime   
)
AS


				
				INSERT INTO [dbo].[Message]
					(
					[Message]
					,[CreatedBy]
					,[CreatedDate]
					,[LastUpdateBy]
					,[LastUpdateDate]
					)
				VALUES
					(
					@Message
					,@CreatedBy
					,@CreatedDate
					,@LastUpdateBy
					,@LastUpdateDate
					)
				-- Get the identity value
				SET @Id = SCOPE_IDENTITY()
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Message_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Message_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Message_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Updates a record in the Message table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Message_Update
(

	@Id int   ,

	@Message varchar (500)  ,

	@CreatedBy varchar (250)  ,

	@CreatedDate datetime   ,

	@LastUpdateBy varchar (250)  ,

	@LastUpdateDate datetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Message]
				SET
					[Message] = @Message
					,[CreatedBy] = @CreatedBy
					,[CreatedDate] = @CreatedDate
					,[LastUpdateBy] = @LastUpdateBy
					,[LastUpdateDate] = @LastUpdateDate
				WHERE
[Id] = @Id 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Message_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Message_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Message_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Deletes a record in the Message table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Message_Delete
(

	@Id int   
)
AS


				DELETE FROM [dbo].[Message] WITH (ROWLOCK) 
				WHERE
					[Id] = @Id
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Message_GetById procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Message_GetById') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Message_GetById
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Select records from the Message table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Message_GetById
(

	@Id int   
)
AS


				SELECT
					[Id],
					[Message],
					[CreatedBy],
					[CreatedDate],
					[LastUpdateBy],
					[LastUpdateDate]
				FROM
					[dbo].[Message]
				WHERE
					[Id] = @Id
				SELECT @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Message_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Message_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Message_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Finds records in the Message table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Message_Find
(

	@SearchUsingOR bit   = null ,

	@Id int   = null ,

	@Message varchar (500)  = null ,

	@CreatedBy varchar (250)  = null ,

	@CreatedDate datetime   = null ,

	@LastUpdateBy varchar (250)  = null ,

	@LastUpdateDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [Id]
	, [Message]
	, [CreatedBy]
	, [CreatedDate]
	, [LastUpdateBy]
	, [LastUpdateDate]
    FROM
	[dbo].[Message]
    WHERE 
	 ([Id] = @Id OR @Id IS NULL)
	AND ([Message] = @Message OR @Message IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
	AND ([LastUpdateBy] = @LastUpdateBy OR @LastUpdateBy IS NULL)
	AND ([LastUpdateDate] = @LastUpdateDate OR @LastUpdateDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [Id]
	, [Message]
	, [CreatedBy]
	, [CreatedDate]
	, [LastUpdateBy]
	, [LastUpdateDate]
    FROM
	[dbo].[Message]
    WHERE 
	 ([Id] = @Id AND @Id is not null)
	OR ([Message] = @Message AND @Message is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([CreatedDate] = @CreatedDate AND @CreatedDate is not null)
	OR ([LastUpdateBy] = @LastUpdateBy AND @LastUpdateBy is not null)
	OR ([LastUpdateDate] = @LastUpdateDate AND @LastUpdateDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Guests_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Guests_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Guests_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Gets all records from the Guests table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Guests_Get_List

AS


				
				SELECT
					[Id],
					[GuestName],
					[CreatedBy],
					[CreatedDate],
					[LastUpdateBy],
					[LastUpdateDate]
				FROM
					[dbo].[Guests]
					
				SELECT @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Guests_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Guests_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Guests_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Gets records from the Guests table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Guests_GetPaged
(

	@WhereClause varchar (8000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[Id]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [Id]'
				SET @SQL = @SQL + ', [GuestName]'
				SET @SQL = @SQL + ', [CreatedBy]'
				SET @SQL = @SQL + ', [CreatedDate]'
				SET @SQL = @SQL + ', [LastUpdateBy]'
				SET @SQL = @SQL + ', [LastUpdateDate]'
				SET @SQL = @SQL + ' FROM [dbo].[Guests]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [Id],'
				SET @SQL = @SQL + ' [GuestName],'
				SET @SQL = @SQL + ' [CreatedBy],'
				SET @SQL = @SQL + ' [CreatedDate],'
				SET @SQL = @SQL + ' [LastUpdateBy],'
				SET @SQL = @SQL + ' [LastUpdateDate]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(1) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[Guests]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Guests_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Guests_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Guests_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Inserts a record into the Guests table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Guests_Insert
(

	@Id int    OUTPUT,

	@GuestName varchar (250)  ,

	@CreatedBy varchar (250)  ,

	@CreatedDate datetime   ,

	@LastUpdateBy varchar (250)  ,

	@LastUpdateDate datetime   
)
AS


				
				INSERT INTO [dbo].[Guests]
					(
					[GuestName]
					,[CreatedBy]
					,[CreatedDate]
					,[LastUpdateBy]
					,[LastUpdateDate]
					)
				VALUES
					(
					@GuestName
					,@CreatedBy
					,@CreatedDate
					,@LastUpdateBy
					,@LastUpdateDate
					)
				-- Get the identity value
				SET @Id = SCOPE_IDENTITY()
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Guests_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Guests_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Guests_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Updates a record in the Guests table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Guests_Update
(

	@Id int   ,

	@GuestName varchar (250)  ,

	@CreatedBy varchar (250)  ,

	@CreatedDate datetime   ,

	@LastUpdateBy varchar (250)  ,

	@LastUpdateDate datetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Guests]
				SET
					[GuestName] = @GuestName
					,[CreatedBy] = @CreatedBy
					,[CreatedDate] = @CreatedDate
					,[LastUpdateBy] = @LastUpdateBy
					,[LastUpdateDate] = @LastUpdateDate
				WHERE
[Id] = @Id 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Guests_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Guests_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Guests_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Deletes a record in the Guests table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Guests_Delete
(

	@Id int   
)
AS


				DELETE FROM [dbo].[Guests] WITH (ROWLOCK) 
				WHERE
					[Id] = @Id
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Guests_GetById procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Guests_GetById') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Guests_GetById
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Select records from the Guests table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Guests_GetById
(

	@Id int   
)
AS


				SELECT
					[Id],
					[GuestName],
					[CreatedBy],
					[CreatedDate],
					[LastUpdateBy],
					[LastUpdateDate]
				FROM
					[dbo].[Guests]
				WHERE
					[Id] = @Id
				SELECT @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Guests_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Guests_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Guests_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: Friday, December 09, 2016

-- Created By:  ()
-- Purpose: Finds records in the Guests table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Guests_Find
(

	@SearchUsingOR bit   = null ,

	@Id int   = null ,

	@GuestName varchar (250)  = null ,

	@CreatedBy varchar (250)  = null ,

	@CreatedDate datetime   = null ,

	@LastUpdateBy varchar (250)  = null ,

	@LastUpdateDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [Id]
	, [GuestName]
	, [CreatedBy]
	, [CreatedDate]
	, [LastUpdateBy]
	, [LastUpdateDate]
    FROM
	[dbo].[Guests]
    WHERE 
	 ([Id] = @Id OR @Id IS NULL)
	AND ([GuestName] = @GuestName OR @GuestName IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
	AND ([LastUpdateBy] = @LastUpdateBy OR @LastUpdateBy IS NULL)
	AND ([LastUpdateDate] = @LastUpdateDate OR @LastUpdateDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [Id]
	, [GuestName]
	, [CreatedBy]
	, [CreatedDate]
	, [LastUpdateBy]
	, [LastUpdateDate]
    FROM
	[dbo].[Guests]
    WHERE 
	 ([Id] = @Id AND @Id is not null)
	OR ([GuestName] = @GuestName AND @GuestName is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([CreatedDate] = @CreatedDate AND @CreatedDate is not null)
	OR ([LastUpdateBy] = @LastUpdateBy AND @LastUpdateBy is not null)
	OR ([LastUpdateDate] = @LastUpdateDate AND @LastUpdateDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET NOCOUNT ON
GO
SET ANSI_NULLS OFF 
GO


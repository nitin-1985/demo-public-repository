﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using ContainerPOC.Entities;
using ContainerPOC.Entities.Validation;

using ContainerPOC.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace ContainerPOC.Services
{		
	/// <summary>
	/// An component type implementation of the 'Guests' table.
	/// </summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class GuestsService : ContainerPOC.Services.GuestsServiceBase
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the GuestsService class.
		/// </summary>
		public GuestsService() : base()
		{
		}
		#endregion Constructors
		
	}//End Class

} // end namespace

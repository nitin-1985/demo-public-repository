﻿using System;
using System.Web.Mvc;
using ContainerPOC.Services;
using ContainerPOC.Entities;

namespace ContainerPOC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult PostData(string name)
        {
            var services = new GuestsService();
            Session["name"] = name;
            Guests gsttable = new Guests
            {
                GuestName = name,
                CreatedBy = name,
                LastUpdateBy = name,
                CreatedDate = DateTime.UtcNow,
                LastUpdateDate = DateTime.UtcNow
            };
            services.Save(gsttable);
            return Json(Url.Action("Dashboard", "Dashboard"));
        }
        public ActionResult Index()
        {
            if (Session["name"] != null)
                return RedirectToAction("Dashboard", "Dashboard");
            return View();
        }
    }
}
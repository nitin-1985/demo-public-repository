﻿using ContainerPOC.Models;
using ContainerPOC.Services;
using System;
using System.Linq;
using System.Web.Mvc;

namespace ContainerPOC.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Dashboard()
        {
            if (Session["name"] == null)
                return RedirectToAction("Index", "Home");
            return View();
        }

        public ActionResult GetData()
        {
            var services = new MessageService();
            var listMessage = services.GetAll().Select(x => new MessageViewModel
            {
                Message = x.Message,
                CreatedBy =Convert.ToString(Session["name"])
            });
            return Json(listMessage, JsonRequestBehavior.AllowGet);
        }
    }
}
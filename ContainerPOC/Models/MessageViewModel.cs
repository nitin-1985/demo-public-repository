﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContainerPOC.Models
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastUpdateBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ContainerPOC.Entities;
using ContainerPOC.Data;

#endregion

namespace ContainerPOC.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MessageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MessageProviderBaseCore : EntityProviderBase<ContainerPOC.Entities.Message, ContainerPOC.Entities.MessageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ContainerPOC.Entities.MessageKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ContainerPOC.Entities.Message Get(TransactionManager transactionManager, ContainerPOC.Entities.MessageKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Message index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="ContainerPOC.Entities.Message"/> class.</returns>
		public ContainerPOC.Entities.Message GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Message index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ContainerPOC.Entities.Message"/> class.</returns>
		public ContainerPOC.Entities.Message GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Message index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ContainerPOC.Entities.Message"/> class.</returns>
		public ContainerPOC.Entities.Message GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Message index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ContainerPOC.Entities.Message"/> class.</returns>
		public ContainerPOC.Entities.Message GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Message index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ContainerPOC.Entities.Message"/> class.</returns>
		public ContainerPOC.Entities.Message GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Message index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ContainerPOC.Entities.Message"/> class.</returns>
		public abstract ContainerPOC.Entities.Message GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Message&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Message&gt;"/></returns>
		public static TList<Message> Fill(IDataReader reader, TList<Message> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ContainerPOC.Entities.Message c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Message")
					.Append("|").Append((System.Int32)reader[((int)MessageColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Message>(
					key.ToString(), // EntityTrackingKey
					"Message",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ContainerPOC.Entities.Message();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)MessageColumn.Id - 1)];
					c.Message = (reader.IsDBNull(((int)MessageColumn.Message - 1)))?null:(System.String)reader[((int)MessageColumn.Message - 1)];
					c.CreatedBy = (reader.IsDBNull(((int)MessageColumn.CreatedBy - 1)))?null:(System.String)reader[((int)MessageColumn.CreatedBy - 1)];
					c.CreatedDate = (reader.IsDBNull(((int)MessageColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)MessageColumn.CreatedDate - 1)];
					c.LastUpdateBy = (reader.IsDBNull(((int)MessageColumn.LastUpdateBy - 1)))?null:(System.String)reader[((int)MessageColumn.LastUpdateBy - 1)];
					c.LastUpdateDate = (reader.IsDBNull(((int)MessageColumn.LastUpdateDate - 1)))?null:(System.DateTime?)reader[((int)MessageColumn.LastUpdateDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ContainerPOC.Entities.Message"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ContainerPOC.Entities.Message"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ContainerPOC.Entities.Message entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)MessageColumn.Id - 1)];
			entity.Message = (reader.IsDBNull(((int)MessageColumn.Message - 1)))?null:(System.String)reader[((int)MessageColumn.Message - 1)];
			entity.CreatedBy = (reader.IsDBNull(((int)MessageColumn.CreatedBy - 1)))?null:(System.String)reader[((int)MessageColumn.CreatedBy - 1)];
			entity.CreatedDate = (reader.IsDBNull(((int)MessageColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)MessageColumn.CreatedDate - 1)];
			entity.LastUpdateBy = (reader.IsDBNull(((int)MessageColumn.LastUpdateBy - 1)))?null:(System.String)reader[((int)MessageColumn.LastUpdateBy - 1)];
			entity.LastUpdateDate = (reader.IsDBNull(((int)MessageColumn.LastUpdateDate - 1)))?null:(System.DateTime?)reader[((int)MessageColumn.LastUpdateDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ContainerPOC.Entities.Message"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ContainerPOC.Entities.Message"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ContainerPOC.Entities.Message entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.Message = Convert.IsDBNull(dataRow["Message"]) ? null : (System.String)dataRow["Message"];
			entity.CreatedBy = Convert.IsDBNull(dataRow["CreatedBy"]) ? null : (System.String)dataRow["CreatedBy"];
			entity.CreatedDate = Convert.IsDBNull(dataRow["CreatedDate"]) ? null : (System.DateTime?)dataRow["CreatedDate"];
			entity.LastUpdateBy = Convert.IsDBNull(dataRow["LastUpdateBy"]) ? null : (System.String)dataRow["LastUpdateBy"];
			entity.LastUpdateDate = Convert.IsDBNull(dataRow["LastUpdateDate"]) ? null : (System.DateTime?)dataRow["LastUpdateDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ContainerPOC.Entities.Message"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ContainerPOC.Entities.Message Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ContainerPOC.Entities.Message entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ContainerPOC.Entities.Message object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ContainerPOC.Entities.Message instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ContainerPOC.Entities.Message Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ContainerPOC.Entities.Message entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MessageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ContainerPOC.Entities.Message</c>
	///</summary>
	public enum MessageChildEntityTypes
	{
	}
	
	#endregion MessageChildEntityTypes
	
	#region MessageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MessageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Message"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageFilterBuilder : SqlFilterBuilder<MessageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageFilterBuilder class.
		/// </summary>
		public MessageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageFilterBuilder
	
	#region MessageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MessageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Message"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageParameterBuilder : ParameterizedSqlFilterBuilder<MessageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageParameterBuilder class.
		/// </summary>
		public MessageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageParameterBuilder
	
	#region MessageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MessageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Message"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MessageSortBuilder : SqlSortBuilder<MessageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageSqlSortBuilder class.
		/// </summary>
		public MessageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MessageSortBuilder
	
} // end namespace

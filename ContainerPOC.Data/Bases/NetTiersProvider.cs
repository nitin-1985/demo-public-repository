﻿#region Using directives

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Configuration.Provider;

using ContainerPOC.Entities;

#endregion

namespace ContainerPOC.Data.Bases
{	
	///<summary>
	/// The base class to implements to create a .NetTiers provider.
	///</summary>
	public abstract class NetTiersProvider : NetTiersProviderBase
	{
		
		///<summary>
		/// Current MessageProviderBase instance.
		///</summary>
		public virtual MessageProviderBase MessageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current GuestsProviderBase instance.
		///</summary>
		public virtual GuestsProviderBase GuestsProvider{get {throw new NotImplementedException();}}
		
		
	}
}
